<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_sbc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f.r~:#L(fQo2Lw?qT@(AS)PJ=k:[+^@9{0-&~,w%{$rWGU<rSWw#Q&B,vpDLK><E');
define('SECURE_AUTH_KEY',  'RGr?53MhzND>L1u~tgZ|2Tah3Pti;&l %tW3b5rW2AiPCC[#|WkcRQ]T5WXxlwIc');
define('LOGGED_IN_KEY',    '%`Nw4<YQhKhgD QqKFIub:?9?YvJ(jiet`+sg6V]%E9^L_5#9 MqY__NYFtEdy1|');
define('NONCE_KEY',        'hR5:PYpJ<^B(; tR| b9P7g%9]%U4oK ^fj]YX)d*d}7q*XK8a(uEb~H]%}#z~}y');
define('AUTH_SALT',        'gC{rNNaH;X1o}2:IL=yeX 7=?U.U)G>jPyagN/;{b%F:RD@~mt-md;l#vL7kFh1l');
define('SECURE_AUTH_SALT', '&:v*X}H4T*2Z.ZDFP<8Q}3|akPu~ep7dea<Wq8Dt)/Lsc]||Gp$JUhHZm )5K,|,');
define('LOGGED_IN_SALT',   'AMVGQ6wJjaR18g.m5:%;E+_$onKMRRCm|n7r69E*(B3(tqBNrx}9h2#|Ab=XrPsr');
define('NONCE_SALT',       ',l{FbH 1]/5V>[x_?cSu!:?Wh#5;X1uMf}lkF_sI0)<bZx#uqyskMRx4/Mmg6ODs');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
